﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using ProductCatalog.Data;

namespace ProductCatalog
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
                            {
                                options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute());
                            });

            services.AddResponseCompression();
            services.AddScoped<StoreDataContext, StoreDataContext>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseStaticFiles();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc(routes =>  
                        {  
                            routes.MapRoute(  
                                name: "default",  
                                template: "{controller=Customer}/{action=Index}/{id?}");  
                        });
            app.UseResponseCompression();
        }
    }
}
