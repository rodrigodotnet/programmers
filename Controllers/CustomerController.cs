using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ProductCatalog.Data;
using ProductCatalog.Models;
using ProductCatalog.ViewModels.CustomersViewModels;

namespace ProductCatalog.Controllers
{
    public class CustomerController : Controller
    {
        private readonly StoreDataContext _context;

        public CustomerController(StoreDataContext context)
        {
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_context.Customers.AsNoTracking().ToList());
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create([Bind] EditorCustomerViewModel model)
        {
            model.Validate();
            if (!model.Invalid)
            {
                var customer = new Customer
                {
                    Name = model.Name,
                    Email = model.Email,
                    Telephone = model.Telephone
                };

                _context.Customers.Add(customer);
                _context.SaveChanges();

                return RedirectToAction("Index");
            }

            foreach (var notify in model.Notifications)
            {
                ModelState.AddModelError(notify.Property, notify.Message);
            }

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var customer = _context.Customers.AsNoTracking().Where(x => x.Id == id).FirstOrDefault();

            if (customer == null)
            {
                return NotFound();
            }
            return View(new EditorCustomerViewModel { Name = customer.Name, Email = customer.Email, Telephone = customer.Telephone });
        }

        [HttpPost]
        public IActionResult Edit(int id, [Bind]EditorCustomerViewModel model)
        {
            if (id != model.Id)
            {
                return NotFound();
            }

            model.Validate();

            if (!model.Invalid)
            {
                var customer = new Customer
                {
                    Id = id,
                    Name = model.Name,
                    Email = model.Email,
                    Telephone = model.Telephone
                };

                _context.Entry(customer).State = EntityState.Modified;
                _context.SaveChanges();

                return RedirectToAction("Index");
            }

            foreach (var notify in model.Notifications)
            {
                ModelState.AddModelError(notify.Property, notify.Message);
            }

            return View(new EditorCustomerViewModel { Name = model.Name, Email = model.Email, Telephone = model.Telephone });
        }

        [HttpGet]
        public IActionResult Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var customer = _context.Customers.AsNoTracking().Where(x => x.Id == id).FirstOrDefault();

            if (customer == null)
            {
                return NotFound();
            }
            return View(new EditorCustomerViewModel { Name = customer.Name, Email = customer.Email, Telephone = customer.Telephone });
        }

        [HttpPost, ActionName("Delete")]
        public IActionResult DeleteConfirmed(int? id)
        {
            var customer = _context.Customers.AsNoTracking().Where(x => x.Id == id).FirstOrDefault();

            if (customer == null)
            {
                return NotFound();
            }

            _context.Customers.Remove(customer);
            _context.SaveChanges();

            return RedirectToAction("Index");
        }


    }
}