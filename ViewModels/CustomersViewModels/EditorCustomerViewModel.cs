using Flunt.Notifications;
using Flunt.Validations;

namespace ProductCatalog.ViewModels.CustomersViewModels
{
    public class EditorCustomerViewModel : Notifiable, IValidatable
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }

        public void Validate()
        {
            AddNotifications(
                new Contract()
                    .HasMaxLen(Name, 200, "Name", "Name must contain up to 120 characters")
                    .HasMinLen(Name, 3, "Name", "Name must contain at least 3 characters")
                    .HasMaxLen(Email, 255, "Email", "Email must contain up to 255 characters")
                    .HasMinLen(Email, 10, "Email", "Email must contain at least 10 characters")
                    .IsEmail(Email,"Email","Invalid email")
                    .HasMaxLen(Telephone, 20, "Telephone", "Telephone must contain up to 20 characters")
                    .HasMinLen(Telephone, 10, "Telephone", "Telephone must contain at least 10 characters")                 
            );
        }
    }
}