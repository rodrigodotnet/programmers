using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ProductCatalog.Models;

namespace ProductCatalog.Data.Maps
{
    public class CustomerMap : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.ToTable("Customer");
            builder.HasKey(x => x.Id);
            builder.Property(x=>x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired().HasMaxLength(200).HasColumnType("varchar(200)");
            builder.Property(x => x.Email).IsRequired().HasMaxLength(255).HasColumnType("varchar(255)");
            builder.Property(x => x.Telephone).IsRequired().HasMaxLength(20).HasColumnType("varchar(20)");
        }
    }
}